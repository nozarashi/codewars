My solutions to [codewars](http://codewars.com) [code katas](https://en.wikipedia.org/wiki/Kata_(programming))

My profile can be found [here](https://www.codewars.com/users/nozarashi)

Languages used:

* Javascript

![My badge](https://www.codewars.com/users/nozarashi/badges/large)