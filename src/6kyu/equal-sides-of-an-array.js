function findEvenIndex(arr) {
  let i = 0;
  while (i < arr.length) {
    let right = arr.reduce((acc, current, index) =>
      index > i ? acc + current : 0
    );
    let left = arr.reduceRight((acc, current, index) =>
      index < i ? acc + current : 0
    );
    if (left == right) {
      return i;
    }
    i++;
  }
  return -1;
}
