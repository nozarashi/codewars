//return the total number of smiling faces in the array
function countSmileys(arr) {
  let smilingFaces = arr.filter((e) => isASmilingFace(e));
  return smilingFaces.length;
}

function isASmilingFace(str) {
  return /^((:|;)(-|~)?(\)|D))$/.test(str);
}
