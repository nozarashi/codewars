function encrypt(text, n) {
  if (n <= 0 || !text) {
    return text;
  }

  let seconds = '';
  let others = '';

  for (let i = 0; i < text.length; i++) {
    if (i % 2 == 1) {
      seconds += text.slice(i, i + 1);
    } else {
      others += text.slice(i, i + 1);
    }
  }
  return encrypt(seconds + others, n - 1);
}

function decrypt(encryptedText, n) {
  if (n <= 0 || !encryptedText) {
    return encryptedText;
  }

  const medium = Math.floor(encryptedText.length / 2);
  //retrouver les chaines aux indices paires
  let seconds = encryptedText.slice(0, medium);
  let others = encryptedText.slice(medium);
  let text = '';
  for (let i = 0; i < seconds.length; i++) {
    text += others[i] + seconds[i];
  }
  text += others.slice(seconds.length);
  return decrypt(text, n - 1);
}
