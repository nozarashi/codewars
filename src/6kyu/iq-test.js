function iqTest(numbers) {
  const allNumbers = numbers.split(' ').map(Number);
  let oddNumbers = allNumbers.filter((number) => number % 2 == 1);
  let evenNumbers = allNumbers.filter((number) => number % 2 == 0);
  let number =
    oddNumbers.length > evenNumbers.length ? evenNumbers[0] : oddNumbers[0];

  return allNumbers.indexOf(number) + 1;
}
