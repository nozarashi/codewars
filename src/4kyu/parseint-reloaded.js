// https://www.codewars.com/kata/525c7c5ab6aecef16e0001a5

// Examples:
// "one" => 1
// "twenty" => 20
// "two hundred forty-six" => 246
// "seven hundred eighty-three thousand nine hundred and nineteen" => 783919
function parseInt(string) {
  const numbers = {
    zero: 0,
    one: 1,
    two: 2,
    three: 3,
    four: 4,
    five: 5,
    six: 6,
    seven: 7,
    eight: 8,
    nine: 9,
    ten: 10,
    eleven: 11,
    twelve: 12,
    thirteen: 13,
    fourteen: 14,
    fifteen: 15,
    sixteen: 16,
    seventeen: 17,
    eighteen: 18,
    nineteen: 19,
    twenty: 20,
    thirty: 30,
    forty: 40,
    fifty: 50,
    sixty: 60,
    seventy: 70,
    eighty: 80,
    ninety: 90,
    hundred: 100,
    thousand: 1000,
    million: 1000000,
  };

  if (string === '' || string === undefined) {
    return 0;
  }

  const str = string.replace(/(\sand\s|-)/g, ' ').trim();
  if (numbers[str] !== undefined) {
    return numbers[str];
  }

  // We must split number by thousands hundreds tens units.

  const regex = /(.* million\s?)?(.* thousand\s?)?(.* hundred\s?)?(.*\s)?(.*)?/;
  // our array look like this: [str, million, thousand, hundred, ten, unit]
  const found = str.match(regex);
  const million =
      found[1] === undefined
        ? undefined
        : found[1].replace('million', '').trim(),
    thousand =
      found[2] === undefined
        ? undefined
        : found[2].replace('thousand', '').trim(),
    hundred =
      found[3] === undefined
        ? undefined
        : found[3].replace('hundred', '').trim(),
    ten = found[4] === undefined ? undefined : found[4].trim(),
    unit = found[5] === undefined ? undefined : found[5].trim();

  return (
    parseInt(million) * 1000000 +
    parseInt(thousand) * 1000 +
    parseInt(hundred) * 100 +
    parseInt(ten) +
    parseInt(unit)
  );
}

console.log(parseInt('one'));
console.log(parseInt('twenty'));
console.log(parseInt('forty six'));
console.log(parseInt('two hundred forty-six'));
console.log(
  parseInt('seven hundred eighty-three thousand nine hundred and nineteen')
);
console.log(parseInt('one million and six thousand'));
