//https://www.codewars.com/kata/51eead3461ccf7db04000017/javascript
function UriBuilder(url) {
  this.params = {};
  this.parseUrl(url);
}

UriBuilder.prototype.parseUrl = function(url) {
  const [root, queryParamsString] = url.split('?');
  this.root = root;
  const queryParamsData = queryParamsString.split('&');
  queryParamsData.forEach(el => {
    const [param, value] = el.split('=');
    this.params[param] = value;
  });
};

UriBuilder.prototype.build = function() {
  let queryParams = [];
  Object.entries(this.params).forEach(([param, value]) => {
    queryParams.push(`${param}=${encodeURI(value)}`);
  });

  return [this.root, queryParams.join('&')].join('?');
}