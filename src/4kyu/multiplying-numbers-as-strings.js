// Principe poser la multiplication comme on le faisait au primaire
function multiply(a, b) {
  let muls = [];
  let l1 = a.length,
    l2 = b.length;
  let carry = 0;
  for (let i = l1 - 1; i >= 0; i--) {
    let tmp = '';

    for (let j = l2 - 1; j >= 0; j--) {
      let n = a.charAt(i) * b.charAt(j) + carry;
      carry = parseInt(n / 10);
      n = n % 10;
      tmp = '' + n + tmp;
    }
    tmp = '' + carry + tmp + '0'.repeat(l1 - 1 - i);
    let s = tmp.replace(/^0*/g, '');
    if (s) {
      muls.push(s);
    }
    carry = 0;
  }
  let sumRes = sum(muls);
  return sumRes;
}

function sum(mulsArr) {
  if (mulsArr.length === 0) {
    return '0';
  }
  let res = '',
    carry = 0;
  mulsArr.sort((a, b) => a.length - b.length);
  let maxLength = mulsArr[mulsArr.length - 1].length;
  for (let i = 0; i < mulsArr.length - 1; i++) {
    let diff = maxLength - mulsArr[i].length;
    mulsArr[i] = '0'.repeat(diff) + mulsArr[i];
  }
  for (let i = maxLength - 1; i >= 0; i--) {
    let x = 0;
    for (let j = 0; j < mulsArr.length; j++) {
      x += parseInt(mulsArr[j].charAt(i));
    }
    x += carry;
    carry = parseInt(x / 10);
    x = x % 10;
    res = '' + x + res;
  }

  res = carry + res;
  return res.replace(/^0*/g, '');
}
