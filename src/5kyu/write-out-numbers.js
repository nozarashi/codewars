function findNumber(n, arr) {
  let res = arr.filter((item) => item.number === n);
  if (res.length > 0) {
    return res[0].word;
  }
  return '';
}
function number2words(n) {
  // works for numbers between 0 and 999999
  const numbers = [
    { number: 0, word: 'zero' },
    { number: 1, word: 'one' },
    { number: 2, word: 'two' },
    { number: 3, word: 'three' },
    { number: 4, word: 'four' },
    { number: 5, word: 'five' },
    { number: 6, word: 'six' },
    { number: 7, word: 'seven' },
    { number: 8, word: 'eight' },
    { number: 9, word: 'nine' },
    { number: 10, word: 'ten' },
    { number: 11, word: 'eleven' },
    { number: 12, word: 'twelve' },
    { number: 13, word: 'thirteen' },
    { number: 14, word: 'fourteen' },
    { number: 15, word: 'fifteen' },
    { number: 16, word: 'sixteen' },
    { number: 17, word: 'seventeen' },
    { number: 18, word: 'eighteen' },
    { number: 19, word: 'nineteen' },
    { number: 20, word: 'twenty' },
    { number: 30, word: 'thirty' },
    { number: 40, word: 'forty' },
    { number: 50, word: 'fifty' },
    { number: 60, word: 'sixty' },
    { number: 70, word: 'seventy' },
    { number: 80, word: 'eighty' },
    { number: 90, word: 'ninety' },
    { number: 100, word: 'one hundred' },
  ];

  let num = findNumber(n, numbers);
  if (num !== '') {
    return num;
  }

  let thousands, hundreds, tens, units;
  units = n % 10;
  tens = (n % 100) - units;
  if (Math.floor(tens / 10) == 1) {
    tens = n % 100;
    units = 0;
  }
  hundreds = Math.floor((n / 100) % 10);
  thousands = Math.floor(n / 1000);

  let word = '';
  let unitsWord = units === 0 ? '' : findNumber(units, numbers);
  let tensWord = tens === 0 ? '' : findNumber(tens, numbers);
  let hundredsWord =
    hundreds === 0 ? '' : findNumber(hundreds, numbers) + ' hundred';

  console.log({ units, tens, hundreds });
  if (tensWord !== '' && unitsWord !== '') {
    unitsWord = '-' + unitsWord;
  }
  word = hundredsWord;
  word += ' ' + tensWord + unitsWord;
  word = word.trim();

  if (thousands === 0) {
    return word;
  }
  return (number2words(thousands) + ' thousand ' + word).trim();
}
