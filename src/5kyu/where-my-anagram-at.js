function anagrams(word, words) {
  return words.filter(
    (w) =>
      w.length == word.length &&
      word.split('').sort().join('') == w.split('').sort().join('')
  );
}
