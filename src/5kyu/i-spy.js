function spyOn(func) {
  // I spy with my little eye… your code here
  let count = 0;
  let argsArray = [];
  let res;
  const spy = function () {
    res = func.call(this, arguments);
    argsArray = [...argsArray, ...Array.from(arguments)];
    count++;
    return res;
  };
  spy.callCount = () => count;
  spy.wasCalledWith = (param) => argsArray.includes(param);
  spy.returned = (n) => {
    console.log(n);
    return n == res;
  };
  return spy;
}
