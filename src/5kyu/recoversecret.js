function recoverSecret(triplets) {
  let predecessors = {};
  // Get all letters

  triplets.forEach((triplet) => {
    let l1 = triplet[0],
      l2 = triplet[1],
      l3 = triplet[2];
    if (predecessors[l1] === undefined) {
      predecessors[l1] = [];
    }
    if (predecessors[l2] === undefined) {
      predecessors[l2] = [];
    }
    if (predecessors[l3] === undefined) {
      predecessors[l3] = [];
    }

    predecessors[l2].push(l1);
    predecessors[l3].push(l1);
    predecessors[l3].push(l2);
  });
  //console.log(predecessors);

  for (let l in predecessors) {
    predecessors[l].forEach((p) => {
      predecessors[l] = predecessors[l].concat(predecessors[p]);
    });
  }
  for (let l in predecessors) {
    predecessors[l].forEach((p) => {
      predecessors[l] = predecessors[l].concat(predecessors[p]);
    });
  }

  //console.log(predecessors);
  let sortable = [];
  for (let l in predecessors) {
    sortable.push([l, uniqueArray(predecessors[l])]);
  }
  sortable.sort((a, b) => a[1].length - b[1].length);
  let secret = sortable.map((e) => e[0]).join('');

  return secret;
}

function uniqueArray(arrArg) {
  return arrArg.filter((elem, pos, arr) => {
    return arr.indexOf(elem) == pos;
  });
}

const triplets1 = [
  ['t', 'u', 'p'],
  ['w', 'h', 'i'],
  ['t', 's', 'u'],
  ['a', 't', 's'],
  ['h', 'a', 'p'],
  ['t', 'i', 's'],
  ['w', 'h', 's'],
];

recoverSecret(triplets1);
