function scramble(str1, str2) {
  let str1Array = str1.split('').sort();
  let str2Array = str2.split('').sort();

  let i = 0,
    j = 0;
  let n1 = str1Array.length,
    n2 = str2Array.length;
  for (i; i < n1 && j < n2; i++) {
    if (str1Array[i] === str2Array[j]) {
      j++;
    }
  }
  return j === n2;
}
