function firstNonRepeatingLetter(s) {
  if (s.length < 2) {
    return s;
  }

  let counts = {};

  // Count occurences
  s.split('').forEach((e) => {
    counts[e.toLowerCase()] = counts[e.toLowerCase()]
      ? counts[e.toLowerCase()] + 1
      : 1;
  });

  for (let letter in counts) {
    if (counts[letter] === 1) {
      return s.indexOf(letter) > -1 ? letter : letter.toUpperCase();
    }
  }
  return '';
}
