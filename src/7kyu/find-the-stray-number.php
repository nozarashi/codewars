<?php 
function stray($arr){
  $occurences = [];
    foreach($arr as $val) {
      if (isset($occurences[$val])) {
          $occurences[$val] += 1;
      } else {
          $occurences[$val] = 1;
      }
    }
    foreach($occurences as $k => $v) {
      if ($v === 1) {
          return $k;
      }
    }
}