function printerError(s) {
  const colorsToUse = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
  ];
  const colors = Array.from(s);
  const errors = colors.filter((color) => colorsToUse.indexOf(color) < 0);

  return errors.length.toString() + '/' + colors.length.toString();
}
