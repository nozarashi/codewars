function GetSum(a, b) {
  const begin = a < b ? a : b;
  const end = a > b ? a : b;
  let sum = 0;
  for (let i = begin; i <= end; i++) {
    sum += i;
  }
  return sum;
}
