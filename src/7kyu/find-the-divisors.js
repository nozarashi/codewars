function divisors(integer) {
  if (integer == 1) {
    return `${integer} is prime`;
  }
  let divisors = [];
  for (let i = 2; i < integer; i++) {
    if (integer % i == 0) {
      divisors.push(i);
    }
  }
  return divisors.length > 0 ? divisors : `${integer} is prime`;
}
